require 'socket'
require 'json'

host = '0.0.0.0'
port = 2345

server = TCPServer.new(host, port)
puts "Running server on #{host}:#{port}"

loop do
  socket = server.accept

  request = socket.gets

  STDERR.puts request


  #response = "Hello World!\n"
  response = {
    :host => Socket.gethostname,
    :timestamp => Time.now
  }.to_json()

  socket.print "HTTP/1.1 200 OK\r\n" +
               "Content-Type: application/json\r\n" +
               "Content-Length: #{response.bytesize}\r\n" +
               "Connection: close\r\n"

  socket.print "\r\n"

  socket.print response

  socket.close
end


